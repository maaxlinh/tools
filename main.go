package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"time"
	"bitbucket.org/maaxlinh/tools/Util"
	"encoding/json"
)

const PORT string = "1808"
const UPLOAD_DIR string = "upload"
const APP_DIR string = "/home/public/linhnq3/go/"
//const DEEPMASK_DIR string = "/home/public/deepmask/"
const DEEPMASK_DIR string = "/home/public/linhnq3/go/src/bitbucket.org/maaxlinh/tools/deepmask/"
//http://1be11add.ngrok.io/api

func main() {
	http.HandleFunc("/api", UploadHandler)
	http.HandleFunc("/video", VideoUploadHandler)

	// serving static files at UPLOAD_DIR directory
	// For example: http://your_host:port/static/1203.jpg
	fs := http.FileServer(http.Dir(UPLOAD_DIR))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	log.Printf("Listen and serve at http://host:%s/api", PORT)
	log.Printf("Listen and serve at http://host:%s/video", PORT)
	log.Fatal(http.ListenAndServe(":"+PORT, nil))
}

func UploadHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")
	if r.Method != "POST" {
		http.Error(w, "Use POST method instead", http.StatusMethodNotAllowed)
		return
	}
	maskNumbers := r.FormValue("np")
	if maskNumbers=="" {
		maskNumbers = "5"
	}
	// parse request
	r.ParseMultipartForm(32 << 20) // left shift, 33554432, 32MB (maximum)
	image, _, err := r.FormFile("image")
	if err != nil {
		log.Println("Bad request:", err.Error())
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	defer image.Close()

	// save to a file with different name
	rand_name := strconv.FormatInt(time.Now().Unix(), 10) + "_" + strconv.Itoa(rand.Intn(1000))
	image_path := filepath.Join(UPLOAD_DIR, rand_name + ".jpg")
	mask_path := filepath.Join(UPLOAD_DIR, rand_name + ".msk")
	if err != nil {
		log.Fatal(err)
	}
	log.Println("image saved : ",APP_DIR+ image_path)
	f, err := os.Create(image_path)
	if err != nil {
		log.Println("Create file failed: ", err.Error())
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	defer f.Close()
	io.Copy(f, image)

	println("-img: "+APP_DIR+image_path)
	println("-mask: "+APP_DIR+mask_path)
	println("-np: "+maskNumbers)
	// do something with the image by bash shell
	cmd := exec.Command("th", DEEPMASK_DIR + "computeProposals.lua", DEEPMASK_DIR + "/pretrained/sharpmask",
		"-img", APP_DIR+image_path,
		"-mask", APP_DIR+mask_path,
		"-np", maskNumbers,
	)
	var output bytes.Buffer
	cmd.Stdout = &output
	if err = cmd.Run(); err != nil {
		log.Println("Execute command failed", err.Error())
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
	log.Println(output.String())


	mask := Util.ReadMask(APP_DIR+mask_path)
	// returns result in JSON message
	//result := fmt.Sprintf(`{"url": "/static/%s"}`, rand_name + ".msk")
	//result := fmt.Sprintf(`{%s}`, mask )
	println("mashal")
	jsonmask, _ := json.Marshal(mask)

	println("json create")
	//fmt.Println(string(jsonmask))
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, string(jsonmask))
}
