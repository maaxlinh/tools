package main

import (
	"net/http"
	"log"
	"math/rand"
	"strconv"
	"time"
	"path/filepath"
	"os"
	"io"
	"errors"
	"os/exec"
	"bytes"
	"strings"
	"io/ioutil"
	"fmt"
)

type APIParams struct {
	id     string
	RLE    string
}
func check(e error) {
	if e != nil {
		panic(e)
	}
}
const FasterRCnn_DIR string = "/home/vietph/Desktop/py-faster-rcnn/tools/"

func VideoUploadHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Use POST method instead", http.StatusMethodNotAllowed)
		return
	}
	video_path, err := SaveVideo(w,r)
	if err != nil {
		log.Println("Save Video Error:", err.Error())
	}

	json_path, err := CallCommanLine(w, r, video_path)
	if err != nil {
		log.Println("Comman Line Error:", err.Error())
	}

	json_data, err := GetResponseData(json_path)

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")

	fmt.Fprint(w, json_data)
}

func GetResponseData (json_path string)(string,error){
	println("reading mask : "+json_path)
	dat, err := ioutil.ReadFile(json_path)
	check(err)
	return string(dat),nil
}

func CallCommanLine(w http.ResponseWriter, r *http.Request, video_path string)(string,error) {

	json_path := strings.Replace(video_path,filepath.Ext(video_path),".json",-1)

	println("--input", APP_DIR+video_path)
	println("--output", APP_DIR+json_path)
	cmd := exec.Command("python", FasterRCnn_DIR + "jsongen_v3.py",
		"--input", APP_DIR+video_path,
		"--output", APP_DIR+json_path,
	)
	var output bytes.Buffer
	cmd.Stdout = &output
	if err := cmd.Run(); err != nil {
		log.Println("Execute command failed", err.Error())
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return "",errors.New("Call command fail!")
	}
	log.Println(output.String())
	return json_path,nil
}

func SaveVideo(w http.ResponseWriter, r *http.Request) (string,error) {
	// parse request
	r.ParseMultipartForm(200 << 20) // left shift, 200MB (maximum)
	video, header, err := r.FormFile("video")
	if err != nil {
		log.Println("Bad request:", err.Error())
		http.Error(w, "Bad request", http.StatusBadRequest)
		return "",errors.New("Cant get file from request!")
	}
	video_extention := filepath.Ext(header.Filename)

	defer video.Close()

	// save to a file with different name
	rand_name := strconv.FormatInt(time.Now().Unix(), 10) + "_" + strconv.Itoa(rand.Intn(1000))
	video_path := filepath.Join(UPLOAD_DIR, rand_name + video_extention)
	f, err := os.Create(video_path)
	if err != nil {
		log.Println("Create file failed: ", err.Error())
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return "",errors.New("Cant save file to Path!")
	}
	defer f.Close()
	io.Copy(f, video)
	log.Println("video saved : ",APP_DIR+ video_path)
	return video_path,nil
}