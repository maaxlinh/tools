package Util

import (
	"io/ioutil"
	"strings"
	"strconv"
)
func check(e error) {
	if e != nil {
		panic(e)
	}
}

type RleData struct {
	id     string
	RLE    string
}

type MaskData struct {
	Height 	string 		`json:"height"`
	Width  	string		`json:"width"`
	Data	[]string	`json:"data"`
}

func ReadMask(inputString string) *MaskData{
	println("reading mask : "+inputString)
	dat, err := ioutil.ReadFile(inputString)
	check(err)
	stringdata := string(dat)
	strdata := strings.Split(stringdata,"\n")
	println("\n strdata len : ",len(strdata))
	header := strings.Split(strdata[0],",")

	strdatanumbers := header[0]
	datanumbers, err := strconv.Atoi(strings.TrimSpace(strdatanumbers))

	var datas []string
	for i := 1; i <= datanumbers; i++ {
		print("\ndata number : ",i)
		datas=append(datas,strdata[i])
		datas[i-1]=Encode(datas[i-1])
	}
	mask := &MaskData{
		Height: header[1],
		Width: header[2],
		Data: datas}
	//fmt.Println(mask)
	//b, err := json.Marshal(mask)
	return mask
	//stringdata = Encode(stringdata)
	//fmt.Print(stringdata)
}