--[[----------------------------------------------------------------------------
Copyright (c) 2016-present, Facebook, Inc. All rights reserved.
This source code is licensed under the BSD-style license found in the
LICENSE file in the root directory of this source tree. An additional grant
of patent rights can be found in the PATENTS file in the same directory.

Run full scene inference in sample image
------------------------------------------------------------------------------]]

require 'torch'
require 'cutorch'
require 'image'
require 'json'

function table.save(  tbl,filename )
  local charS,charE = "   ","\n"
  local file,err = io.open( filename, "wb" )
  if err then return err end

  -- initiate variables for save procedure
  local tables,lookup = { tbl },{ [tbl] = 1 }
  file:write( "return {"..charE )

  for idx,t in ipairs( tables ) do
    file:write( "-- Table: {"..idx.."}"..charE )
    file:write( "{"..charE )
    local thandled = {}

    for i,v in ipairs( t ) do
      thandled[i] = true
      local stype = type( v )
      -- only handle value
      if stype == "table" then
        if not lookup[v] then
          table.insert( tables, v )
          lookup[v] = #tables
        end
        file:write( charS.."{"..lookup[v].."},"..charE )
      elseif stype == "string" then
        file:write(  charS..exportstring( v )..","..charE )
      elseif stype == "number" then
        file:write(  charS..tostring( v )..","..charE )
      end
    end

    for i,v in pairs( t ) do
      -- escape handled values
      if (not thandled[i]) then

        local str = ""
        local stype = type( i )
        -- handle index
        if stype == "table" then
          if not lookup[i] then
            table.insert( tables,i )
            lookup[i] = #tables
          end
          str = charS.."[{"..lookup[i].."}]="
        elseif stype == "string" then
          str = charS.."["..exportstring( i ).."]="
        elseif stype == "number" then
          str = charS.."["..tostring( i ).."]="
        end

        if str ~= "" then
          stype = type( v )
          -- handle value
          if stype == "table" then
            if not lookup[v] then
              table.insert( tables,v )
              lookup[v] = #tables
            end
            file:write( str.."{"..lookup[v].."},"..charE )
          elseif stype == "string" then
            file:write( str..exportstring( v )..","..charE )
          elseif stype == "number" then
            file:write( str..tostring( v )..","..charE )
          end
        end
      end
    end
    file:write( "},"..charE )
  end
  file:write( "}" )
  file:close()
end
--------------------------------------------------------------------------------
-- parse arguments
local cmd = torch.CmdLine()
cmd:text()
cmd:text('evaluate deepmask/sharpmask')
cmd:text()
cmd:argument('-model', 'path to model to load')
cmd:text('Options:')
cmd:option('-img','data/testImage.jpg' ,'path/to/test/image')
cmd:option('-mask','data/mask.msk' ,'path/to/mask')
cmd:option('-gpu', 1, 'gpu device')
cmd:option('-np', 5,'number of proposals to save in test')
cmd:option('-si', -2.5, 'initial scale')
cmd:option('-sf', .5, 'final scale')
cmd:option('-ss', .5, 'scale step')
cmd:option('-dm', false, 'use DeepMask version of SharpMask')

local config = cmd:parse(arg)

--------------------------------------------------------------------------------
-- various initializations
torch.setdefaulttensortype('torch.FloatTensor')
cutorch.setDevice(config.gpu)

local coco = require 'coco'
local maskApi = coco.MaskApi

local meanstd = {mean = { 0.485, 0.456, 0.406 }, std = { 0.229, 0.224, 0.225 }}

--------------------------------------------------------------------------------
-- load moodel
paths.dofile('DeepMask.lua')
paths.dofile('SharpMask.lua')

print('| loading model file... ' .. config.model)
local m = torch.load(config.model..'/model.t7')
local model = m.model
model:inference(config.np)
model:cuda()

--------------------------------------------------------------------------------
-- create inference module
local scales = {}
for i = config.si,config.sf,config.ss do table.insert(scales,2^i) end

if torch.type(model)=='nn.DeepMask' then
  paths.dofile('InferDeepMask.lua')
elseif torch.type(model)=='nn.SharpMask' then
  paths.dofile('InferSharpMask.lua')
end

local infer = Infer{
  np = config.np,
  scales = scales,
  meanstd = meanstd,
  model = model,
  dm = config.dm,
}
myDrawMasks = function(path, img, masks, maxn, alpha, clrs )
    assert(img:isContiguous() and img:dim()==3)
    local n, h, w = masks:size(1), masks:size(2), masks:size(3)
          if not maxn then maxn=n end
    if not alpha then alpha=.4 end
    if not clrs then clrs=torch.rand(n,3)*.6+.4 end
         file = io.open(path, "w")
  file:write(string.format("%d,%d,%d",n,h,w))

          for i=1,math.min(maxn,n) do
            file:write("\n")
      local M = masks[i]:contiguous():data()
            local B = torch.ByteTensor(h,w):zero():contiguous():data()
            -- get boundaries B in masks M quickly
            for y=0,h-2 do for x=0,w-2 do
        local k=y*w+x
  --Add a comment to this line
        if M[k]~=M[k+1] then B[k],B[k+1]=1,1 end
        if M[k]~=M[k+w] then B[k],B[k+w]=1,1 end
        if M[k]~=M[k+1+w] then B[k],B[k+1+w]=1,1 end
      end end
      -- softly embed masks into image and add solid boundaries
      for j=1,1 do
        local O,c,a = img[j]:data(), clrs[i][j], alpha
              for k=0,w*h-1 do if M[k]==1 then O[k]=O[k]*(1-a)+c*a file:write("1") else file:write("0") end end
        for k=0,w*h-1 do if B[k]==1 then O[k]=c end end
      end
  	--for k=0,w*h-1 do if M[k]==1 then  end end
    end

    -- appends a word test to the last line of the file

    file:close()
  end
--------------------------------------------------------------------------------
-- do it
print('| start')

-- load image
local img = image.load(config.img)
local h,w = img:size(2),img:size(3)

-- forward all scales
infer:forward(img)

-- get top propsals
local masks,_ = infer:getTopProps(.2,h,w)

--table.save(masks,config.mask)

--local Rs = maskApi.encode(masks)
-- Opens a file in read
--file = io.open(config.mask, "w")

-- appends a word test to the last line of the file
--file:write(string.format("%s",masks))

-- closes the open file
--file:close()

-- save result
local res = img:clone()
--maskApi.drawMasks(res, masks, 10)
myDrawMasks(config.mask,res, masks)
image.save(string.format('./res.jpg',config.model),res)

print('| done')
collectgarbage()

