import dlib
import cv2

def detectFace(img, face_tracker, tracking_face):
    face_cascade = cv2.CascadeClassifier("./haarcascades/haarcascade_frontalface_default.xml")
    eye_cascade = cv2.CascadeClassifier("./haarcascades/parojosG.xml")
    mouth_cascade = cv2.CascadeClassifier("./haarcascades/Mouth.xml")
    face_window = (0,0,0,0)
    eye_window = (0, 0, 0, 0)
    mouth_window = (0, 0, 0, 0)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    if not tracking_face:
        faces = face_cascade.detectMultiScale(gray, 1.3, 4)

        biggestFace = 0
        X = 0
        Y = 0
        W = 0
        H = 0
        roi_color = []
        for (x,y,w,h) in faces:
            if (w * h > biggestFace):
                X = int (x)
                Y = int (y)
                W = int (w)
                H = int (h)
                biggestFace = w * h
        if biggestFace > 0:
            face_tracker.start_track(img, dlib.rectangle(X, Y, X + W, Y + H))
            tracking_face = 1

    if tracking_face:
        tracking_quality = face_tracker.update(img)
        if (tracking_quality >= 5.5):
            tracked_position = face_tracker.get_position()
            rec_X = int(tracked_position.left())
            rec_Y = int(tracked_position.top())
            rec_W = int(tracked_position.width())
            rec_H = int(tracked_position.height())

            #cv2.rectangle(img, (rec_X, rec_Y), (rec_X + rec_W, rec_Y + rec_H), (255, 0, 0), 2)
            roi_gray = gray[rec_Y:rec_Y + rec_H, rec_X:rec_X + rec_W]
            roi_color = img[rec_Y:rec_Y + rec_H, rec_X:rec_X + rec_W]
            face_window = (int(rec_X), int(rec_Y), int(rec_X + rec_W), int(rec_Y + rec_H))
            eyes = eye_cascade.detectMultiScale(roi_gray)
            if len(eyes) > 0:
                (e_x,e_y,e_w,e_h) = eyes[0]
                eye_window = (int(rec_X + e_x), int(rec_Y + e_y), int(rec_X + e_x + e_w), int(rec_Y + e_y + e_h))

            mouth = mouth_cascade.detectMultiScale(roi_gray)
            if len(mouth) > 0:
                (m_x, m_y, m_w, m_h) = mouth[0]
                mouth_window = (int(rec_X + m_x), int(rec_Y + m_y), int(rec_X + m_x + m_w), int(rec_Y + m_y + m_h))
                #cv2.rectangle(roi_color, (m_x, m_y), (m_x + m_w, m_y + m_h), (0,0,255), 2)
        else:
            tracking_face = 0
    return (face_tracker, tracking_face, face_window, eye_window, mouth_window)