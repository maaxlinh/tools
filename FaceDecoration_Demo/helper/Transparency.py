import cv2
import numpy as np

def loadTransparentImage(img_name):
    img = cv2.imread(img_name, cv2.IMREAD_UNCHANGED)
    rgb_channel = img[:, :, :3]
    alpha_channel = img[:, :, 3]

    # Alpha factor
    alpha_factor = alpha_channel[:, :, np.newaxis].astype(np.float32) / 255.0
    alpha_factor = np.concatenate((alpha_factor, alpha_factor, alpha_factor), axis=2)
    transparent_rgb = rgb_channel.astype(np.float32) * alpha_factor

    return alpha_factor, transparent_rgb