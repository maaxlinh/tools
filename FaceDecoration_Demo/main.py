import cv2
import os
import glob
import argparse
import time
import dlib
import numpy as np

import helper.FaceDetection as FaceDetection
import helper.Transparency as Transparency
'''
Computation time helper function
'''

def StartStep(str):
    global global_start
    global_start = time.clock()
    print ("Start Step:", str)
def EndStep():
    print ("Time Cost: ", time.clock() - global_start, "seconds")
    print()

if __name__ == "__main__":
    start = time.clock()

    glass_alpha_factor, glass_transparent_rgb = Transparency.loadTransparentImage("decoratingImage/eye_glass.png")
    ears_alpha_factor, ears_transparent_rgb = Transparency.loadTransparentImage("decoratingImage/bunny_ears.png")
    beards_alpha_factor, beards_transparent_rgb = Transparency.loadTransparentImage("decoratingImage/beard.png")
    whisker_alpha_factor, whisker_transparent_rgb = Transparency.loadTransparentImage("decoratingImage/whisker.png")

    face_tracker = dlib.correlation_tracker()
    tracking_face = 0

    print("-------------------")
    print ("START!")
    cam = cv2.VideoCapture(0)
    window_show = False
    #videoWriter = cv2.VideoWriter('output.avi', -1, 5.0, (640, 480))

    print("DETECT AND TRACKING FACE FROM VIDEO")
    while cam.isOpened():
        s, img = cam.read()
        if s:
            face_tracker, tracking_face, face_window, eye_window, mouth_window = FaceDetection.detectFace(img, face_tracker, tracking_face)
            if not eye_window[0] == 0:
                #attach glass into video
                if window_show:
                    cv2.rectangle(img, (eye_window[0], eye_window[1]), (eye_window[2], eye_window[3]), (0, 255, 0), 2)
                else:
                    resized_glass_rgb = cv2.resize(glass_transparent_rgb, (eye_window[2] - eye_window[0], eye_window[3] - eye_window[1]), interpolation = cv2.INTER_CUBIC)
                    img[eye_window[1]:eye_window[3], eye_window[0]:eye_window[2]] += resized_glass_rgb.astype(np.uint8)
            if not face_window[0] == 0:
                if window_show:
                    cv2.rectangle(img, (face_window[0], face_window[1]),(face_window[2], face_window[3]), (255, 0, 0), 2)
                else:
                    # attach bunny ear
                    ears_size = face_window[2] - face_window[0]
                    resized_ears_rgb = cv2.resize(ears_transparent_rgb, (ears_size, ears_size), interpolation=cv2.INTER_CUBIC)
                    resized_ears_alpha_factor = cv2.resize(ears_alpha_factor, (ears_size, ears_size), interpolation=cv2.INTER_CUBIC)
                    roi_ears = img[max(face_window[1] - ears_size, 0): face_window[1],15 + face_window[0]: 15 + face_window[2]]
                    roi_ears = resized_ears_rgb.astype(np.uint8)[max(0, ears_size - face_window[1]): ears_size, :, :] + (1 - resized_ears_alpha_factor[max(0, ears_size - face_window[1]): ears_size,:,:]) * roi_ears
                    img[max(face_window[1] - ears_size, 0): face_window[1], 15 + face_window[0]: 15 + face_window[2]] = roi_ears

                    # attach cat whisker
                    whisker_size = (face_window[2] - face_window[0], face_window[3] - face_window[1])
                    resized_whisker_rgb = cv2.resize(whisker_transparent_rgb, whisker_size, interpolation=cv2.INTER_CUBIC)
                    resized_whisker_alpha_factor = cv2.resize(whisker_alpha_factor, whisker_size,interpolation=cv2.INTER_CUBIC)
                    roi_whisker = img[face_window[1] + 20: face_window[3] + 20, face_window[0]: face_window[2]]
                    roi_whisker = resized_whisker_rgb.astype(np.uint8) + (1 - resized_whisker_alpha_factor) * roi_whisker
                    img[face_window[1] + 20: face_window[3] + 20, face_window[0]: face_window[2]] = roi_whisker

                    # attach beards
                    # beards_size = int((face_window[2] - face_window[0])/4)
                    # resized_beards_rgb = cv2.resize(beards_transparent_rgb, (beards_size * 2, beards_size * 2), interpolation=cv2.INTER_CUBIC)
                    # resized_beards_alpha_factor = cv2.resize(beards_alpha_factor, (beards_size * 2, beards_size * 2), interpolation=cv2.INTER_CUBIC)
                    # roi_beards = img[face_window[3] - 2 * beards_size + 20: face_window[3] + 20,face_window[0] + beards_size: face_window[0] + 3 * beards_size]
                    # roi_beards = resized_beards_rgb.astype(np.uint8) + roi_beards * (1 - resized_beards_alpha_factor)
                    # img[face_window[3] - 2 * beards_size + 20: face_window[3] + 20, face_window[0] + beards_size: face_window[0] + 3 * beards_size] = roi_beards
            # write the flipped frame
            #videoWriter.write(img)
            cv2.imshow('origin', img)
            c = cv2.waitKey(1) & 0xFF
            if c == ord('w'):
                window_show = False
                print("PUT ON BUNNY EARS, GLASSES, WHISKER!")
            if c == ord('q'):
                break
        else:
            break

    cam.release()
    #videoWriter.release()
    cv2.destroyAllWindows()

    print("END!")
    print("-------------------")