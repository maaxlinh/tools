#file server API
* get image upload from web client
* call deepmask command
* binary mask to RLE
* send back to webclient

## Keys
* image: upload image
* up: mask number

## Usage
```
$ mkdir -p go/upload
$ cd go
$ export GOPATH=$(pwd)
$ go get bitbucket.org/maaxlinh/tools
$ ./bin/uploadapi
```